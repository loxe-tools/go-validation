package custom

import (
	"flag"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal/generate/input"
)

func parse(defaultValues input.Arguments) input.Arguments {
	help := "use --help instead"
	flag.StringVar(&defaultValues.SourcePackage, input.MetaᐸArgumentsᐳ.SourcePackage, defaultValues.SourcePackage, help)
	flag.StringVar(&defaultValues.SourceType, input.MetaᐸArgumentsᐳ.SourceType, defaultValues.SourceType, help)

	flag.StringVar(&defaultValues.OutputFileName, input.MetaᐸArgumentsᐳ.OutputFileName, defaultValues.OutputFileName, help)
	flag.StringVar(&defaultValues.OutputFolderPath, input.MetaᐸArgumentsᐳ.OutputFolderPath, defaultValues.OutputFolderPath, help)
	flag.StringVar(&defaultValues.OutputPackage, input.MetaᐸArgumentsᐳ.OutputPackage, defaultValues.OutputPackage, help)
	flag.StringVar(&defaultValues.OutputPackageName, input.MetaᐸArgumentsᐳ.OutputPackageName, defaultValues.OutputPackageName, help)
	flag.BoolVar(&defaultValues.Client, input.MetaᐸArgumentsᐳ.Client, defaultValues.Client, help)

	flag.BoolVar(&defaultValues.Debug, input.MetaᐸArgumentsᐳ.Debug, defaultValues.Debug, help)
	flag.BoolVar(&defaultValues.SupportsANSI, input.MetaᐸArgumentsᐳ.SupportsANSI, defaultValues.SupportsANSI, help)
	flag.BoolVar(&defaultValues.OnlyAsciiTypeNames, input.MetaᐸArgumentsᐳ.OnlyAsciiTypeNames, defaultValues.OnlyAsciiTypeNames, help)
	flag.StringVar(&defaultValues.Workdir, input.MetaᐸArgumentsᐳ.Workdir, defaultValues.Workdir, help)

	flag.Usage = cliUsage
	flag.Parse()

	log := logCLI.NewLogCLI(defaultValues.Debug, defaultValues.SupportsANSI)
	input.PrintArguments(defaultValues, log)

	return defaultValues
}
