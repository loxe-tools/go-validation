package custom

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/brand"
	"gitlab.com/loxe-tools/go-validation/internal"
	"gitlab.com/loxe-tools/go-validation/internal/generate"
	"gitlab.com/loxe-tools/go-validation/internal/generate/input"
)

type Arguments = input.Arguments

func DefaultArgsGenerate(fieldNameMetafier ...func(string) string) {
	Generate(input.DefaultArguments, fieldNameMetafier...)
}
func Generate(defaultArgs Arguments, fieldNameMetafier ...func(string) string) {
	brand.ToStdout(fmt.Sprintf("%s %s", cliTitle, internal.LibraryModuleVersion))
	generate.Generate(parse(defaultArgs), fieldNameMetafier...)
}

const cliTitle = "Löxe Validation - Schema generator"
