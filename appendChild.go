package loxe

func (v *LoxeValidator) AppendChild(fieldName string, child *LoxeValidator) {
	_, alreadyExists := v.childs[fieldName]
	if alreadyExists {
		return
	}
	v.childs[fieldName] = child
}
