package loxe

func (v *LoxeValidator) ApplyRootError(e error) {
	if e == nil {
		return
	}
	v.rootError = e
}
