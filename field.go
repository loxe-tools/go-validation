package loxe

func (v *LoxeValidator) Field(fieldName string) error {
	err, exists :=  v.fields[fieldName]
	if !exists {
		return nil
	}
	return err
}
