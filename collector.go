package loxe

import (
	"encoding/json"
	"fmt"
)

type Collector = func(e error) interface{}

func Collect(v *LoxeValidator, c Collector) *CollectedForm {
	if v == nil {
		return nil
	}

	return iterateRawValidator(v, c)
}

// -----

type CollectedForm struct {
	RootError interface{}
	Fields    map[string]interface{}
	Childs    map[string]*CollectedForm
}

func (c *CollectedForm) MarshalJSON() ([]byte, error) {
	rootErr := "null"
	if c.RootError != nil {
		j, e := json.Marshal(c.RootError)
		if e != nil {
			return nil, e
		}
		rootErr = string(j)
	}

	fields := "null"
	if len(c.Fields) != 0 {
		j, e := json.Marshal(c.Fields)
		if e != nil {
			return nil, e
		}
		fields = string(j)
	}

	childs := "null"
	if len(c.Childs) != 0 {
		j, e := json.Marshal(c.Childs)
		if e != nil {
			return nil, e
		}
		childs = string(j)
	}

	j := fmt.Sprintf(loxeValidatorErrorJson,
		rootErrorJsonField, rootErr,
		fieldsJsonField, fields,
		childsJsonField, childs,
	)
	return []byte(j), nil
}
