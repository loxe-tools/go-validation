package loxe

import (
	"encoding/json"
	"fmt"
)

func (v *LoxeValidator) MarshalJSON() ([]byte, error) {
	rootErr := "null"
	if v.rootError != nil {
		j, e := json.Marshal(v.rootError)
		if e != nil {
			return nil, e
		}
		rootErr = string(j)
	}

	fields := "null"
	if len(v.fields) != 0 {
		j, e := json.Marshal(v.fields)
		if e != nil {
			return nil, e
		}
		fields = string(j)
	}

	childs := "null"
	if len(v.childs) != 0 {
		j, e := json.Marshal(v.childs)
		if e != nil {
			return nil, e
		}
		childs = string(j)
	}

	j := fmt.Sprintf(loxeValidatorErrorJson,
		rootErrorJsonField, rootErr,
		fieldsJsonField, fields,
		childsJsonField, childs,
	)
	return []byte(j), nil
}

const loxeValidatorErrorJson = `{ "%s": %s, "%s": %s, "%s": %s }`
const rootErrorJsonField = "rootError"
const fieldsJsonField = "fields"
const childsJsonField = "childs"
