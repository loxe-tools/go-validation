package internal

// LibraryModulePath is the GO import path to the library.
//
// Since this lib only supports GO modules, it is the same
// name of the library GO module
const LibraryModulePath = "gitlab.com/loxe-tools/go-validation"

// LibraryModuleVersion is the version of the library.
//
// It must be in sync with the actual library version,
// since it will be used as a debug information inside
// generated files, for example
const LibraryModuleVersion = "v1.0.41"
