package generate

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal/generate/input"
)

func createParser(arguments input.Arguments, log *logCLI.LogCLI) *goParser.GoParser {
	parserLog := log.Info("Parsing source package to filter not eligible types")
	config := goParser.Config{
		Dir:   arguments.Workdir,
		Focus: resolveFocus(arguments),
	}
	parser, e := goParser.NewGoParser(arguments.SourcePackage, config, *parserLog)
	if e != nil {
		log.Fatal("Error creating GO parser: %v", e)
	}

	return parser
}

func resolveFocus(arguments input.Arguments) *goParser.ParserFocus {
	pkgFocus := goParser.FocusPackagePath(arguments.SourcePackage)
	if arguments.SourceType != "" {
		return goParser.FocusMerge(pkgFocus, goParser.FocusTypeName(arguments.SourceType))
	}
	return pkgFocus
}
