package serverSide

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser/helpers"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/types"
)

func StructIsEligible(struct_ *types.TypeName, outputPackageName, outputPackagePath string, log *logCLI.LogCLI) bool {
	if !helpers.ObjectIsAccessible(struct_, outputPackagePath, log) {
		log.Debug("Since this struct is not accessible from package '%s %s', it will be skipped",
			outputPackageName, outputPackagePath)
		return false
	}

	structType := struct_.Type().Underlying().(*types.Struct)
	if structType.NumFields() == 0 {
		log.Debug("Skipped (doesn't contain any fields)")
		return false
	}

	if !helpers.StructFieldsFullyAccessible(structType, outputPackagePath, log) {
		log.Debug("Since this struct contains fields not fully accessible from package '%s %s', it will be skipped",
			outputPackageName, outputPackagePath)
		return false
	}

	return true
}
