package serverSide

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal"
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide/templates"
	"go/types"
)

// structState is a map that tells if a field is ambiguous AND if the child type/method has already been created
func (c *serverSideCoder) codeStructFields(struct_ *types.Struct, structTypeIdentifier string, structState map[string]string, log *logCLI.LogCLI) structFieldsValues {
	structFieldsLog := log.Debug("Generating struct code field-by-field...")
	values := structFieldsValues{}
	//if struct_.NumFields() == 0 {
	// should never occur (handled by newCodeCallback)
	//}

	for i := 0; i < struct_.NumFields(); i++ {
		field := struct_.Field(i)
		fieldName := field.Name()
		fieldLog := structFieldsLog.Debug("Field '%s'...", fieldName)
		fieldTypeIdentifier := ResolveTypeIdentifier(field, c.file, fieldLog)
		loxeImportAlias := c.file.AliasFromPath(internal.LibraryModulePath)

		existentField, fieldAlreadyExists := structState[fieldName]
		if fieldAlreadyExists {
			fieldLog.Fatal("The field '%s %s', is ambiguous with '%s %s'. Fix it to continue",
				fieldName, fieldTypeIdentifier, fieldName, existentField)
		}
		structState[fieldName] = fieldTypeIdentifier

		values.metaTypeCode += fmt.Sprintf("%s string\n", fieldName)
		values.metaFieldsCode += fmt.Sprintf("%s: \"%s\",\n", fieldName, templates.FieldNameMetafy(fieldName))

		fieldMethodCode, e := templates.SchemaFieldsMethod(
			fieldName, fieldTypeIdentifier, structTypeIdentifier, loxeImportAlias)
		if e != nil {
			fieldLog.Fatal("%v", e)
		}
		fieldLog.Debug("Schema/TestSchema field method code generated")

		rootedFieldMethodCode, e := templates.SchemaRootedFieldsMethod(
			fieldName, structTypeIdentifier, loxeImportAlias)
		if e != nil {
			fieldLog.Fatal("%v", e)
		}
		fieldLog.Debug("Schema/TestSchema rooted field method code generated")

		helpersCode, e := c.codeRuleHelpers(fieldTypeIdentifier, loxeImportAlias)
		if e != nil {
			fieldLog.Fatal("%v", e)
		}

		values.code += fieldMethodCode
		values.code += rootedFieldMethodCode
		values.code += helpersCode

		childStruct, isStructType := field.Type().Underlying().(*types.Struct)
		if !isStructType {
			fieldLog.Debug("Not a struct, done. Go to the next one...")
			continue
		}
		structLog := fieldLog.Debug("Is a struct, additional code is required...")

		_, schemaChildsMethodBuilt := structState["## CHILD ##"]
		if !schemaChildsMethodBuilt {
			schemaChildsMethod, e := templates.SchemaChilds(structTypeIdentifier, loxeImportAlias)
			if e != nil {
				structLog.Fatal("%v", e)
			}
			values.code += schemaChildsMethod
			structState["## CHILD ##"] = "code already generated"
			structLog.Debug("Schema/TestSchema child method code generated")
		}

		childFieldMethod, e := templates.SchemaChildsMethod(
			fieldName, fieldTypeIdentifier, structTypeIdentifier, loxeImportAlias)
		if e != nil {
			structLog.Fatal("%v", e)
		}
		structLog.Debug("Schema/TestSchema child field method code generated")
		values.code += childFieldMethod

		// Indirect recursion
		nestedStruct := c.codeStruct(field,
			structLog.Debug("Recursively generating code for the nested struct field type '%s'...", fieldTypeIdentifier))
		values.code += nestedStruct

		if !field.Embedded() {
			structLog.Debug("Struct is not embedded, done. Go to the next one...")
			continue
		}

		embeddedLog := structLog.Debug("Recursively generating code for the embedded struct field type '%s'...",
			fieldTypeIdentifier)
		embedded := c.codeStructFields(childStruct, structTypeIdentifier, structState, embeddedLog)
		values.code += embedded.code
		values.metaTypeCode += embedded.metaTypeCode
		values.metaFieldsCode += embedded.metaFieldsCode
		embeddedLog.Debug("Code for the embedded struct created")
		embeddedLog.Debug("Done. Go to the next one...")
	}

	return values
}

type structFieldsValues struct {
	code           string
	metaTypeCode   string
	metaFieldsCode string
}
