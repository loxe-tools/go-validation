package serverSide

func (c *serverSideCoder) newTypeBuilt(typeIdentifier string) {
	c.builtTypes[typeIdentifier] = true
}

func (c *serverSideCoder) isTypeBuilt(typeIdentifier string) bool {
	_, alreadyBuilt := c.builtTypes[typeIdentifier]
	return alreadyBuilt
}
