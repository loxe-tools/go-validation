package serverSide

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal"
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide/templates"
	"go/types"
)

func (c *serverSideCoder) codeStruct(struct_ types.Object, log *logCLI.LogCLI) string {
	structTypeIdentifier := ResolveTypeIdentifier(struct_, c.file, log)
	if c.isTypeBuilt(structTypeIdentifier) {
		log.Debug("Code for this struct already generated, go to the next one...")
		return ""
	}

	c.newTypeBuilt(structTypeIdentifier)
	structType := struct_.Type().Underlying().(*types.Struct)
	loxeImportAlias := c.file.AliasFromPath(internal.LibraryModulePath)

	schemaCode, e := templates.Schema(structTypeIdentifier, loxeImportAlias)
	if e != nil {
		log.Fatal("%v", e)
	}
	log.Debug("Schema/TestSchema types with RootRule, RootedFields and Fields methods code generated")

	structFieldsCode := c.codeStructFields(structType, structTypeIdentifier, map[string]string{}, log)
	log.Debug("Schema/TestSchema fields methods code generated")

	structHelpersCode, e := c.codeRuleHelpers(structTypeIdentifier, loxeImportAlias)
	if e != nil {
		log.Fatal("%v", e)
	}

	return schemaCode + "\n" +
		structFieldsCode.code + "\n" +
		structHelpersCode + "\n" +
		fmt.Sprintf("var %s = struct {\n", templates.StructMetaVarName(structTypeIdentifier)) +
		structFieldsCode.metaTypeCode + "}{\n" +
		structFieldsCode.metaFieldsCode + "}\n"
}
