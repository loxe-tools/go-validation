package serverSide

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/types"
)

func NewCodeCallback(fileName, outputPackageName, outputPackagePath string) (*goFile.GoFile, func(struct_ *types.TypeName, parentLog *logCLI.LogCLI) error) {
	c := newServerSideCoder(fileName, outputPackageName, outputPackagePath)

	return c.file, func(struct_ *types.TypeName, parentLog *logCLI.LogCLI) error {
		log := parentLog.Debug("Analysing *types.Struct '%s'...", struct_.Name())
		if !StructIsEligible(struct_, outputPackageName, outputPackagePath, log) {
			return nil
		}

		code := c.codeStruct(struct_, log.Debug("Generating code for the struct '%s'...", struct_.Name()))
		c.file.AddCode(code)
		log.Info("Code for the struct '%s' generated", struct_.Name())
		return nil
	}
}
