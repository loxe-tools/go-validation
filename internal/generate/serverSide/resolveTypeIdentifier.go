package serverSide

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile"
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser/helpers"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"go/types"
)

// all imports needs to have a unique alias, relative to the entire package,
// to guarantee that the generated typename's will be consistent through all the
// generated files
func ResolveTypeIdentifier(obj types.Object, i *goFile.GoFile, parentLog *logCLI.LogCLI) string {
	log := parentLog.Debug("Resolving type identifier...")
	callback := func(typename *types.Named) {
		obj := typename.Obj()
		if i.NeedImport(obj.Pkg().Path()) {
			i.AddImport(obj.Pkg().Name(), obj.Pkg().Path())
		}
	}
	helpers.CallbackOnNamedType(obj.Type(), callback, log)
	return helpers.ResolveTypeIdentifier(obj.Type(), i, log)
}
