package serverSide

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goFile/goImports"
	"gitlab.com/loxe-tools/go-validation/internal"
)

const rootPackageName = "validator"

// baseImports will a minimal imports that a file needs to
// have in order to compile and be functional.
//
// packageImportPath: the import path to the package that the
// FileImports will belong to.
func baseImports(packageImportPath string) *goImports.GoImports {
	i := goImports.NewGoImports(packageImportPath)
	i.AddImport(rootPackageName, internal.LibraryModulePath)
	i.AddImport("testing", "testing")
	return i
}
