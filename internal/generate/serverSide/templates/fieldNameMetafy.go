package templates

import (
	"fmt"
	"strings"
)

// FieldNameMetafy is the function that transforms a field
// name to another string representation. Example:
//
// If some struct field has the name "FieldA", one can create
// a function to transform this name to "field_a".
//
// This function is used to transform the struct field name to
// a validation form field name
//
// Note that this is not the real implementation, this function
// will just call the private one, the one that can be replaced
// at runtime.
//
// To avoid inconsistency, when this function is used for the
// first time it will set a private boolean to true. When this
// boolean is set to true, the implementation cannot be replaced
func FieldNameMetafy(fieldName string) string {
	fieldNameMetafyExecuted = true
	return fieldNameMetafy(fieldName)
}

// fieldNameMetafyExecuted is boolean that
// will be true if the fieldNameMetafy function
// was already executed
var fieldNameMetafyExecuted = false

// fieldNameMetafy is the real implementation of the exported
// FieldNameMetafy.
//
// Note that this function can be replaced at runtime, since it
// is a var, just do it before generating any code. To do it, use
// the SetFieldNameMetafy exported function
var fieldNameMetafy = func(fieldName string) string {
	return strings.ToLower(fieldName[0:1]) + fieldName[1:]
}

// SetFieldNameMetafy is used to change the fieldNameMetafy
// implementation at runtime.
//
// If the function cannot be changed, an error will be returned
func SetFieldNameMetafy(newImplementation func(string) string) error {
	if fieldNameMetafyExecuted {
		return fmt.Errorf("the function 'FieldNameMetafy' was already executed. You can only change it's implementation before the first usage")
	}

	fieldNameMetafy = newImplementation
	return nil
}
