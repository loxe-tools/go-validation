package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

func Schema(structTypeIdentifier, loxeValidatorImportAlias string) (string, error) {
	type tmplVars struct {
		NewSchemaFnName                   string
		NewTestSchemaFnName               string
		SchemaTypeName                    string
		TestSchemaTypeName                string
		StructTypeIdentifier              string
		LoxeValidatorTypeIdentifier       string
		NewLoxeValidatorFnIdentifier      string
		LoxeValidatorApplyErrorMethodName string
		LoxeValidatorErrorMethodName      string
		SchemaRootRuleMethodName          string
		SchemaFieldsTypeName              string
		TestSchemaFieldsTypeName          string
		SchemaFieldsMethodName            string
		SchemaRootedFieldsTypeName        string
		TestSchemaRootedFieldsTypeName    string
		SchemaRootedFieldsMethodName      string
		TestRuleStructTypeName            string
		TestSuiteTypeIdentifier     string
	}
	vars := tmplVars{
		newSchemaFnName(structTypeIdentifier),
		newTestSchemaFnName(structTypeIdentifier),
		schemaTypeName(structTypeIdentifier),
		testSchemaTypeName(structTypeIdentifier),
		structTypeIdentifier,
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, loxeValidatorTypeName),
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, newLoxeValidatorFnName),
		applyRootErrorMethodName,
		rootErrorMethodName,
		rootRuleMethodName,
		schemaFieldsTypeName(structTypeIdentifier),
		testSchemaFieldsTypeName(structTypeIdentifier),
		schemaFieldsMethodName,
		schemaRootedFieldsTypeName(structTypeIdentifier),
		testSchemaRootedFieldsTypeName(structTypeIdentifier),
		schemaRootedFieldsMethodName,
		testRuleTypeName(structTypeIdentifier),
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, testSuiteTypeName),
	}

	var data bytes.Buffer
	e := schema.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

// -----

var schema = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplSchema)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

const rootRuleMethodName = "RootRule"

func schemaTypeName(structTypeIdentifier string) string {
	return "Schema" + identifierToTypeName("<"+structTypeIdentifier+">")
}
func testSchemaTypeName(structTypeIdentifier string) string {
	return "Test" + schemaTypeName(structTypeIdentifier)
}
func newSchemaFnName(structTypeIdentifier string) string {
	return "New" + schemaTypeName(structTypeIdentifier)
}
func newTestSchemaFnName(structTypeIdentifier string) string {
	return "New" + testSchemaTypeName(structTypeIdentifier)
}

const tmplSchema = `
func {{.NewSchemaFnName}}() {{.SchemaTypeName}} {
	return func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
		return {{.NewLoxeValidatorFnIdentifier}}()
	}
}

func {{.NewTestSchemaFnName}}() {{.TestSchemaTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {}
}

type {{.SchemaTypeName}} func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}}
type {{.TestSchemaTypeName}} func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}})
type {{.SchemaFieldsTypeName}} func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}}
type {{.TestSchemaFieldsTypeName}} func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}})
type {{.SchemaRootedFieldsTypeName}} func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}}
type {{.TestSchemaRootedFieldsTypeName}} func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}})

func (prevFn {{.SchemaTypeName}}) {{.SchemaRootRuleMethodName}}(rule func({{.StructTypeIdentifier}}) error) {{.SchemaTypeName}} {
	return func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
		v := prevFn(struct_)
		v.{{.LoxeValidatorApplyErrorMethodName}}(rule(struct_))
		return v
	}
}

func (prevFn {{.TestSchemaTypeName}}) {{.SchemaRootRuleMethodName}}(test {{.TestRuleStructTypeName}}) {{.TestSchemaTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {
		prevFn(s, schema)
		test(s, func(value {{.StructTypeIdentifier}}) error {
			return schema(value).{{.LoxeValidatorErrorMethodName}}()
		})
	}
}

func (prevFn {{.SchemaTypeName}}) {{.SchemaFieldsMethodName}}(callback func(schema {{.SchemaFieldsTypeName}}) {{.SchemaFieldsTypeName}}) {{.SchemaTypeName}} {
	return func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
		v := prevFn(struct_)
		return callback(func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
			return v
		})(struct_)
	}
}

func (prevFn {{.TestSchemaTypeName}}) {{.SchemaFieldsMethodName}}(callback func({{.TestSchemaFieldsTypeName}}) {{.TestSchemaFieldsTypeName}}) {{.TestSchemaTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {
		prevFn(s, schema)
		callback(func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {})(s, schema)
	}
}

func (prevFn {{.SchemaTypeName}}) {{.SchemaRootedFieldsMethodName}}(callback func(schema {{.SchemaRootedFieldsTypeName}}) {{.SchemaRootedFieldsTypeName}}) {{.SchemaTypeName}} {
	return func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
		v := prevFn(struct_)
		return callback(func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
			return v
		})(struct_)
	}
}

func (prevFn {{.TestSchemaTypeName}}) RootedFields(callback func({{.TestSchemaRootedFieldsTypeName}}) {{.TestSchemaRootedFieldsTypeName}}) {{.TestSchemaTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {
		prevFn(s, schema)
		callback(func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {})(s, schema)
	}
}
`
