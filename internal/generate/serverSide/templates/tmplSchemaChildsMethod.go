package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

func SchemaChildsMethod(fieldName, fieldTypeIdentifier, structTypeIdentifier, loxeValidatorImportAlias string) (string, error) {
	type tmplVars struct {
		SchemaTypeName                     string
		SchemaChildsTypeName               string
		TestSchemaChildsTypeName           string
		FieldTypeIdentifier                string
		FieldName                          string
		FieldMetaName                      string
		LoxeValidatorTypeIdentifier        string
		LoxeValidatorAppendChildMethodName string
		StructTypeIdentifier               string
		ChildFieldSchemaTypeName           string
		ChildFieldTestSchemaTypeName       string
		TestSuiteTypeIdentifier            string
	}
	vars := tmplVars{
		schemaTypeName(structTypeIdentifier),
		schemaChildsTypeName(structTypeIdentifier),
		testSchemaChildsTypeName(structTypeIdentifier),
		fieldTypeIdentifier,
		fieldName,
		fmt.Sprintf("%s.%s", StructMetaVarName(structTypeIdentifier), fieldName),
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, loxeValidatorTypeName),
		appendChildMethodName,
		structTypeIdentifier,
		schemaTypeName(fieldTypeIdentifier),
		testSchemaTypeName(fieldTypeIdentifier),
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, testSuiteTypeName),
	}

	var data bytes.Buffer
	e := schemaChildsMethod.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

// -----

var schemaChildsMethod = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplSchemaChildsMethod)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

const tmplSchemaChildsMethod = `
func (prevFn {{.SchemaChildsTypeName}}) {{.FieldName}}(child {{.ChildFieldSchemaTypeName}}) {{.SchemaChildsTypeName}} {
	return func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
		v := prevFn(struct_)
		v.{{.LoxeValidatorAppendChildMethodName}}({{.FieldMetaName}}, child(struct_.{{.FieldName}}))
		return v
	}
}

func (prevFn {{.TestSchemaChildsTypeName}}) {{.FieldName}}(testSchema {{.ChildFieldTestSchemaTypeName}}) {{.TestSchemaChildsTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeName}}) {
		prevFn(s, schema)

		testSchema(s, func(struct_ {{.FieldTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
			aux_variable := {{.StructTypeIdentifier}}{}
			aux_variable.{{.FieldName}} = struct_
			v := schema(aux_variable)

			return v
		})
	}
}
`
