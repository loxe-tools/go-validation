package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

func SchemaFieldsMethod(fieldName, fieldTypeIdentifier, structTypeIdentifier, loxeValidatorImportAlias string) (string, error) {
	type tmplVars struct {
		SchemaFieldsTypeName                 string
		TestSchemaFieldsTypeName             string
		StructTypeIdentifier                 string
		SchemaTypeIdentifier                 string
		FieldTypeIdentifier                  string
		LoxeValidatorTypeIdentifier          string
		LoxeValidatorSetFieldErrorMethodName string
		LoxeValidatorFieldMethodName         string
		FieldName                            string
		FieldMetaName                        string
		TestRuleTypeName                     string
		TestSuiteTypeIdentifier              string
	}
	vars := tmplVars{
		schemaFieldsTypeName(structTypeIdentifier),
		testSchemaFieldsTypeName(structTypeIdentifier),
		structTypeIdentifier,
		schemaTypeName(structTypeIdentifier),
		fieldTypeIdentifier,
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, loxeValidatorTypeName),
		applyFieldErrorMethodName,
		fieldMethodName,
		fieldName,
		fmt.Sprintf("%s.%s", StructMetaVarName(structTypeIdentifier), fieldName),
		testRuleTypeName(fieldTypeIdentifier),
		fmt.Sprintf("%s.%s", loxeValidatorImportAlias, testSuiteTypeName),
	}

	var data bytes.Buffer
	e := schemaFieldsMethod.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

// -----

var schemaFieldsMethod = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplSchemaFieldsMethod)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

const schemaFieldsMethodName = "Fields"

func schemaFieldsTypeName(structTypeIdentifier string) string {
	return "SchemaFields" + identifierToTypeName("<"+structTypeIdentifier+">")
}
func testSchemaFieldsTypeName(structTypeIdentifier string) string {
	return "Test" + schemaFieldsTypeName(structTypeIdentifier)
}

const tmplSchemaFieldsMethod = `
func (prevFn {{.SchemaFieldsTypeName}}) {{.FieldName}}(rule func({{.FieldTypeIdentifier}}) error) {{.SchemaFieldsTypeName}} {
	return func(struct_ {{.StructTypeIdentifier}}) *{{.LoxeValidatorTypeIdentifier}} {
		v := prevFn(struct_)
		v.{{.LoxeValidatorSetFieldErrorMethodName}}({{.FieldMetaName}}, rule(struct_.{{.FieldName}}))
		return v
	}
}

func (prevFn {{.TestSchemaFieldsTypeName}}) {{.FieldName}}(test {{.TestRuleTypeName}}) {{.TestSchemaFieldsTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, schema {{.SchemaTypeIdentifier}}) {
		prevFn(s, schema)
		test(s, func(value {{.FieldTypeIdentifier}}) error {
			aux_variable := {{.StructTypeIdentifier}}{}
			aux_variable.{{.FieldName}} = value
			return schema(aux_variable).{{.LoxeValidatorFieldMethodName}}({{.FieldMetaName}})
		})
	}
}
`
