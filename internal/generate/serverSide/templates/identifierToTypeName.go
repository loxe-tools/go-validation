package templates

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser/helpers"
)

func SetIdentifierToTypeName(newImplementation func(string) string) error {
	if identifierToTypeNameExecuted {
		return fmt.Errorf("the function 'IdentifierToTypeName' was already executed. You can only change it's implementation before the first usage")
	}

	identifierToTypeName = func(typeIdentifier string) string {
		identifierToTypeNameExecuted = true
		return newImplementation(typeIdentifier)
	}
	return nil
}

// -----

var identifierToTypeName = func(typeIdentifier string) string {
	identifierToTypeNameExecuted = true
	return helpers.IdentifierToTypeName(typeIdentifier)
}

var identifierToTypeNameExecuted = false
