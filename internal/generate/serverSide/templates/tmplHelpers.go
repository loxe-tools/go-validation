package templates

import (
	"bytes"
	"text/template"
)

func Helpers(typeIdentifier string) (string, error) {
	type tmplVars struct {
		IntersectionFnName string
		UnionFnName        string
		NotFnName          string
		TypeIdentifier     string
	}
	vars := tmplVars{
		intersectionFnName(typeIdentifier),
		unionFnName(typeIdentifier),
		notFnName(typeIdentifier),
		typeIdentifier,
	}

	var data bytes.Buffer
	e := helpers_.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

// -----

// named with "_" to avoid collision with some imports in this package
var helpers_ = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplHelpersRule)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

func intersectionFnName(typeIdentifier string) string {
	return "Intersection" + identifierToTypeName("<"+typeIdentifier+">")
}
func unionFnName(typeIdentifier string) string {
	return "Union" + identifierToTypeName("<"+typeIdentifier+">")
}
func notFnName(typeIdentifier string) string {
	return "Not" + identifierToTypeName("<"+typeIdentifier+">")
}

const tmplHelpersRule = `
func {{.IntersectionFnName}}(rules ...func({{.TypeIdentifier}}) error) func({{.TypeIdentifier}}) error {
	return func(s {{.TypeIdentifier}}) error {
		for _, currRule := range rules {
			e := currRule(s)
			if e != nil {
				return e
			}
		}
		return nil
	}
}

func {{.UnionFnName}}(rules ...func({{.TypeIdentifier}}) error) func({{.TypeIdentifier}}) error {
	return func(s {{.TypeIdentifier}}) error {
		var e error
		for _, currRule := range rules {
			e = currRule(s)
			if e == nil {
				return nil
			}
		}
		return e
	}
}

func {{.NotFnName}}(rule func({{.TypeIdentifier}}) error, newError error) func({{.TypeIdentifier}}) error {
	return func(s {{.TypeIdentifier}}) error {
		e := rule(s)
		if e == nil {
			return newError
		}
		return nil
	}
}
`
