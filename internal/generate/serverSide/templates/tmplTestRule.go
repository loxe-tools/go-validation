package templates

import (
	"bytes"
	"fmt"
	"text/template"
)

func TestRule(typeIdentifier, importAlias string) (string, error) {
	type tmplVars struct {
		NewTestRuleFnName             string
		TestRuleTypeName              string
		RuleTypeIdentifier            string
		TestRulePassMethodName        string
		TestRuleFailMethodName        string
		ComparableErrorTypeIdentifier string
		ComparableErrorCmpMethodName  string
		TestSuiteTypeIdentifier string
		TestSuiteHelperMethodName string
		TestSuiteFatalfMethodName string
	}
	vars := tmplVars{
		newTestRuleFnName(typeIdentifier),
		testRuleTypeName(typeIdentifier),
		typeIdentifier,
		testRulePassMethodName,
		testRuleFailMethodName,
		fmt.Sprintf("%s.%s", importAlias, comparableErrorTypeName),
		comparableErrorIsMethodName,
		fmt.Sprintf("%s.%s", importAlias, testSuiteTypeName),
		testSuiteHelperMethodName,
		testSuiteFatalfMethodName,
	}

	var data bytes.Buffer
	e := testRule.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}

// -----

var testRule = func() *template.Template {
	tmpl, e := template.New("").Parse(tmplTestRule)
	if e != nil {
		panic(e)
	}

	return tmpl
}()

const testRulePassMethodName = "Pass"
const testRuleFailMethodName = "Fail"

func testRuleTypeName(typeIdentifier string) string {
	return "TestRule" + identifierToTypeName("<" + typeIdentifier + ">")
}
func newTestRuleFnName(typeIdentifier string) string {
	return "New" + testRuleTypeName(typeIdentifier)
}

const tmplTestRule = `
func {{.NewTestRuleFnName}}() {{.TestRuleTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, rule func({{.RuleTypeIdentifier}}) error) {}
}

type {{.TestRuleTypeName}} func(s {{.TestSuiteTypeIdentifier}}, rule func({{.RuleTypeIdentifier}}) error)

func (prevFn {{.TestRuleTypeName}}) {{.TestRulePassMethodName}}(value {{.RuleTypeIdentifier}}) {{.TestRuleTypeName}} {
	return func(s {{.TestSuiteTypeIdentifier}}, rule func({{.RuleTypeIdentifier}}) error) {
		prevFn(s, rule)

		e := rule(value)
		s.{{.TestSuiteHelperMethodName}}()
		if e != nil {
			s.{{.TestSuiteFatalfMethodName}}("Value '%+v' should be considered valid\n\tError returned: %+v", value, e)
			return
		}
	}
}

func (prevFn {{.TestRuleTypeName}}) {{.TestRuleFailMethodName}}(value {{.RuleTypeIdentifier}}, expectedError {{.ComparableErrorTypeIdentifier}}) {{.TestRuleTypeName}} {
	if expectedError == nil {
		return prevFn
	}

	return func(s {{.TestSuiteTypeIdentifier}}, rule func({{.RuleTypeIdentifier}}) error) {
		prevFn(s, rule)

		e := rule(value)
		s.{{.TestSuiteHelperMethodName}}()
		if e == nil {
			s.{{.TestSuiteFatalfMethodName}}("Value '%+v' should be considered invalid\n\tError returned: nil", value)
			return
		}
		if !expectedError.{{.ComparableErrorCmpMethodName}}(e) {
			s.{{.TestSuiteFatalfMethodName}}("Value '%+v' should be considered invalid\n\tError returned: %+v\n\tError expected: %+v", value, e, expectedError)
			return
		}
	}
}
`
