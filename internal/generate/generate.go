package generate

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal/generate/clientSide"
	"gitlab.com/loxe-tools/go-validation/internal/generate/input"
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide"
	"go/types"
)

func Generate(arguments input.Arguments, fieldNameMetafy ...func(string) string) {
	log := logCLI.NewLogCLI(arguments.Debug, arguments.SupportsANSI)
	arguments = handleParams(arguments, log, fieldNameMetafy...)

	parser := createParser(arguments, log.Info("Parsing source package to filter not eligible types"))
	var file fileI
	var callback func(struct_ *types.TypeName, parentLog *logCLI.LogCLI) error
	if arguments.Client {
		file, callback = clientSide.NewClientCodeCallback(arguments.OutputFileName, arguments.OutputPackageName, arguments.OutputPackage)
	} else {
		file, callback = serverSide.NewCodeCallback(arguments.OutputFileName, arguments.OutputPackageName, arguments.OutputPackage)
	}

	e := parser.IterateFileStructs(callback)
	if e != nil {
		log.Fatal("%v", e)
	}

	saveFile(file, arguments, log.Info("Saving files to disk..."))
	log.Info("The end!")
	return
}
