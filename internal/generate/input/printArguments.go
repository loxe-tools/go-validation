package input

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
)

func PrintArguments(args Arguments, log *logCLI.LogCLI) {
	log.Debug(fmt.Sprintf(argsTmpl,
		MetaᐸArgumentsᐳ.SourcePackage, args.SourcePackage,
		MetaᐸArgumentsᐳ.SourceType, args.SourceType,

		MetaᐸArgumentsᐳ.OutputFileName, args.OutputFileName,
		MetaᐸArgumentsᐳ.OutputFolderPath, args.OutputFolderPath,
		MetaᐸArgumentsᐳ.OutputPackage, args.OutputPackage,
		MetaᐸArgumentsᐳ.OutputPackageName, args.OutputPackageName,
		MetaᐸArgumentsᐳ.Client, args.Client,

		MetaᐸArgumentsᐳ.Debug, args.Debug,
		MetaᐸArgumentsᐳ.SupportsANSI, args.SupportsANSI,
		MetaᐸArgumentsᐳ.OnlyAsciiTypeNames, args.OnlyAsciiTypeNames,
		MetaᐸArgumentsᐳ.Workdir, args.Workdir,
	))
}

const argsTmpl = `CLI arguments:
Source arguments:
	--%s: %s
	--%s: %s
Output arguments:
	--%s: %s
	--%s: %s
	--%s: %s
	--%s: %s
	--%s: %t
Other arguments:
	--%s: %t
	--%s: %t
	--%s: %t
	--%s: %s`
