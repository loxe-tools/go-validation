/*
█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
██                                                                                                                 ██
██                                  ▄██▄        ▄██▄                                                               ██
██                                 ██████      ██████                                                              ██
██        ████                      ▀██▀        ▀██▀                                                               ██
██        ████                                                                                                     ██
██        ████                       ▄████████████▄          ██████       ██████          ▄████████████▄           ██
██        ████                      ████████████████          ██████     ██████          ████████████████▄         ██
██        ████                    ▄█████▀      ▀█████▄         ██████   ██████          █████▀       ▀█████        ██
██        ████                    █████          █████          ██████ ██████          ████████████████████        ██
██        ████                    █████          █████           ███████████           █████                       ██
██        ████                     █████▄      ▄█████             █████████             █████▄       ▄█████        ██
██        ███████████████▄          ▀██████████████▀               ███████               ▀███████████████▀         ██
██        █████████████████▄          ▀██████████▀                █████████                ▀███████████▀           ██
██                                                               ███████████                                       ██
██                                                              ██████ ██████                                      ██
█████████████████████████████████████████████████████████      ██████   ██████      █████████████████████████████████
                                                              ██████     ██████
                                                             ██████       ██████

                            Löxe - Criação e Desenvolvimento (Santa Catarina, Brasil)
                     FILE GENERATED AUTOMATICALLY BY gitlab.com/loxe-tools/go-validation v1.0.39
*/
package input

import (
	validator "gitlab.com/loxe-tools/go-validation"
)

func NewSchemaᐸArgumentsᐳ() SchemaᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		return validator.NewLoxeValidator()
	}
}

func NewTestSchemaᐸArgumentsᐳ() TestSchemaᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {}
}

type SchemaᐸArgumentsᐳ func(struct_ Arguments) *validator.LoxeValidator
type TestSchemaᐸArgumentsᐳ func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ)
type SchemaFieldsᐸArgumentsᐳ func(struct_ Arguments) *validator.LoxeValidator
type TestSchemaFieldsᐸArgumentsᐳ func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ)
type SchemaRootedFieldsᐸArgumentsᐳ func(struct_ Arguments) *validator.LoxeValidator
type TestSchemaRootedFieldsᐸArgumentsᐳ func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ)

func (prevFn SchemaᐸArgumentsᐳ) RootRule(rule func(Arguments) error) SchemaᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyRootError(rule(struct_))
		return v
	}
}

func (prevFn TestSchemaᐸArgumentsᐳ) RootRule(test TestRuleᐸArgumentsᐳ) TestSchemaᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).RootError()
		})
	}
}

func (prevFn SchemaᐸArgumentsᐳ) Fields(callback func(schema SchemaFieldsᐸArgumentsᐳ) SchemaFieldsᐸArgumentsᐳ) SchemaᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		return callback(func(struct_ Arguments) *validator.LoxeValidator {
			return v
		})(struct_)
	}
}

func (prevFn TestSchemaᐸArgumentsᐳ) Fields(callback func(TestSchemaFieldsᐸArgumentsᐳ) TestSchemaFieldsᐸArgumentsᐳ) TestSchemaᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		callback(func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {})(s, schema)
	}
}

func (prevFn SchemaᐸArgumentsᐳ) RootedFields(callback func(schema SchemaRootedFieldsᐸArgumentsᐳ) SchemaRootedFieldsᐸArgumentsᐳ) SchemaᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		return callback(func(struct_ Arguments) *validator.LoxeValidator {
			return v
		})(struct_)
	}
}

func (prevFn TestSchemaᐸArgumentsᐳ) RootedFields(callback func(TestSchemaRootedFieldsᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ) TestSchemaᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		callback(func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {})(s, schema)
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) SourcePackage(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SourcePackage, rule(struct_.SourcePackage))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) SourcePackage(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.SourcePackage = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.SourcePackage)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) SourcePackage(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SourcePackage, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) SourcePackage(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.SourcePackage)
		})
	}
}

func NewTestRuleᐸstringᐳ() TestRuleᐸstringᐳ {
	return func(s validator.TestSuite, rule func(string) error) {}
}

type TestRuleᐸstringᐳ func(s validator.TestSuite, rule func(string) error)

func (prevFn TestRuleᐸstringᐳ) Pass(value string) TestRuleᐸstringᐳ {
	return func(s validator.TestSuite, rule func(string) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e != nil {
			s.Fatalf("Value '%+v' should be considered valid\n\tError returned: %+v", value, e)
			return
		}
	}
}

func (prevFn TestRuleᐸstringᐳ) Fail(value string, expectedError validator.ComparableError) TestRuleᐸstringᐳ {
	if expectedError == nil {
		return prevFn
	}

	return func(s validator.TestSuite, rule func(string) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e == nil {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: nil", value)
			return
		}
		if !expectedError.Is(e) {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: %+v\n\tError expected: %+v", value, e, expectedError)
			return
		}
	}
}

func Intersectionᐸstringᐳ(rules ...func(string) error) func(string) error {
	return func(s string) error {
		for _, currRule := range rules {
			e := currRule(s)
			if e != nil {
				return e
			}
		}
		return nil
	}
}

func Unionᐸstringᐳ(rules ...func(string) error) func(string) error {
	return func(s string) error {
		var e error
		for _, currRule := range rules {
			e = currRule(s)
			if e == nil {
				return nil
			}
		}
		return e
	}
}

func Notᐸstringᐳ(rule func(string) error, newError error) func(string) error {
	return func(s string) error {
		e := rule(s)
		if e == nil {
			return newError
		}
		return nil
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) SourceType(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SourceType, rule(struct_.SourceType))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) SourceType(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.SourceType = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.SourceType)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) SourceType(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SourceType, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) SourceType(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.SourceType)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) OutputFileName(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OutputFileName, rule(struct_.OutputFileName))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) OutputFileName(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.OutputFileName = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.OutputFileName)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) OutputFileName(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OutputFileName, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) OutputFileName(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.OutputFileName)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) OutputFolderPath(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OutputFolderPath, rule(struct_.OutputFolderPath))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) OutputFolderPath(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.OutputFolderPath = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.OutputFolderPath)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) OutputFolderPath(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OutputFolderPath, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) OutputFolderPath(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.OutputFolderPath)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) OutputPackage(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OutputPackage, rule(struct_.OutputPackage))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) OutputPackage(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.OutputPackage = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.OutputPackage)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) OutputPackage(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OutputPackage, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) OutputPackage(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.OutputPackage)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) OutputPackageName(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OutputPackageName, rule(struct_.OutputPackageName))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) OutputPackageName(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.OutputPackageName = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.OutputPackageName)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) OutputPackageName(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OutputPackageName, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) OutputPackageName(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.OutputPackageName)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) Client(rule func(bool) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.Client, rule(struct_.Client))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) Client(test TestRuleᐸboolᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value bool) error {
			aux_variable := Arguments{}
			aux_variable.Client = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.Client)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) Client(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.Client, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) Client(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.Client)
		})
	}
}

func NewTestRuleᐸboolᐳ() TestRuleᐸboolᐳ {
	return func(s validator.TestSuite, rule func(bool) error) {}
}

type TestRuleᐸboolᐳ func(s validator.TestSuite, rule func(bool) error)

func (prevFn TestRuleᐸboolᐳ) Pass(value bool) TestRuleᐸboolᐳ {
	return func(s validator.TestSuite, rule func(bool) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e != nil {
			s.Fatalf("Value '%+v' should be considered valid\n\tError returned: %+v", value, e)
			return
		}
	}
}

func (prevFn TestRuleᐸboolᐳ) Fail(value bool, expectedError validator.ComparableError) TestRuleᐸboolᐳ {
	if expectedError == nil {
		return prevFn
	}

	return func(s validator.TestSuite, rule func(bool) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e == nil {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: nil", value)
			return
		}
		if !expectedError.Is(e) {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: %+v\n\tError expected: %+v", value, e, expectedError)
			return
		}
	}
}

func Intersectionᐸboolᐳ(rules ...func(bool) error) func(bool) error {
	return func(s bool) error {
		for _, currRule := range rules {
			e := currRule(s)
			if e != nil {
				return e
			}
		}
		return nil
	}
}

func Unionᐸboolᐳ(rules ...func(bool) error) func(bool) error {
	return func(s bool) error {
		var e error
		for _, currRule := range rules {
			e = currRule(s)
			if e == nil {
				return nil
			}
		}
		return e
	}
}

func Notᐸboolᐳ(rule func(bool) error, newError error) func(bool) error {
	return func(s bool) error {
		e := rule(s)
		if e == nil {
			return newError
		}
		return nil
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) Debug(rule func(bool) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.Debug, rule(struct_.Debug))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) Debug(test TestRuleᐸboolᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value bool) error {
			aux_variable := Arguments{}
			aux_variable.Debug = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.Debug)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) Debug(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.Debug, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) Debug(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.Debug)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) SupportsANSI(rule func(bool) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SupportsANSI, rule(struct_.SupportsANSI))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) SupportsANSI(test TestRuleᐸboolᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value bool) error {
			aux_variable := Arguments{}
			aux_variable.SupportsANSI = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.SupportsANSI)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) SupportsANSI(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.SupportsANSI, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) SupportsANSI(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.SupportsANSI)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) OnlyAsciiTypeNames(rule func(bool) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OnlyAsciiTypeNames, rule(struct_.OnlyAsciiTypeNames))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) OnlyAsciiTypeNames(test TestRuleᐸboolᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value bool) error {
			aux_variable := Arguments{}
			aux_variable.OnlyAsciiTypeNames = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.OnlyAsciiTypeNames)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) OnlyAsciiTypeNames(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.OnlyAsciiTypeNames, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) OnlyAsciiTypeNames(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.OnlyAsciiTypeNames)
		})
	}
}

func (prevFn SchemaFieldsᐸArgumentsᐳ) Workdir(rule func(string) error) SchemaFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.Workdir, rule(struct_.Workdir))
		return v
	}
}

func (prevFn TestSchemaFieldsᐸArgumentsᐳ) Workdir(test TestRuleᐸstringᐳ) TestSchemaFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value string) error {
			aux_variable := Arguments{}
			aux_variable.Workdir = value
			return schema(aux_variable).Field(MetaᐸArgumentsᐳ.Workdir)
		})
	}
}

func (prevFn SchemaRootedFieldsᐸArgumentsᐳ) Workdir(rule func(Arguments) error) SchemaRootedFieldsᐸArgumentsᐳ {
	return func(struct_ Arguments) *validator.LoxeValidator {
		v := prevFn(struct_)
		v.ApplyFieldError(MetaᐸArgumentsᐳ.Workdir, rule(struct_))
		return v
	}
}

func (prevFn TestSchemaRootedFieldsᐸArgumentsᐳ) Workdir(test TestRuleᐸArgumentsᐳ) TestSchemaRootedFieldsᐸArgumentsᐳ {
	return func(s validator.TestSuite, schema SchemaᐸArgumentsᐳ) {
		prevFn(s, schema)
		test(s, func(value Arguments) error {
			return schema(value).Field(MetaᐸArgumentsᐳ.Workdir)
		})
	}
}

func NewTestRuleᐸArgumentsᐳ() TestRuleᐸArgumentsᐳ {
	return func(s validator.TestSuite, rule func(Arguments) error) {}
}

type TestRuleᐸArgumentsᐳ func(s validator.TestSuite, rule func(Arguments) error)

func (prevFn TestRuleᐸArgumentsᐳ) Pass(value Arguments) TestRuleᐸArgumentsᐳ {
	return func(s validator.TestSuite, rule func(Arguments) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e != nil {
			s.Fatalf("Value '%+v' should be considered valid\n\tError returned: %+v", value, e)
			return
		}
	}
}

func (prevFn TestRuleᐸArgumentsᐳ) Fail(value Arguments, expectedError validator.ComparableError) TestRuleᐸArgumentsᐳ {
	if expectedError == nil {
		return prevFn
	}

	return func(s validator.TestSuite, rule func(Arguments) error) {
		prevFn(s, rule)

		e := rule(value)
		s.Helper()
		if e == nil {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: nil", value)
			return
		}
		if !expectedError.Is(e) {
			s.Fatalf("Value '%+v' should be considered invalid\n\tError returned: %+v\n\tError expected: %+v", value, e, expectedError)
			return
		}
	}
}

func IntersectionᐸArgumentsᐳ(rules ...func(Arguments) error) func(Arguments) error {
	return func(s Arguments) error {
		for _, currRule := range rules {
			e := currRule(s)
			if e != nil {
				return e
			}
		}
		return nil
	}
}

func UnionᐸArgumentsᐳ(rules ...func(Arguments) error) func(Arguments) error {
	return func(s Arguments) error {
		var e error
		for _, currRule := range rules {
			e = currRule(s)
			if e == nil {
				return nil
			}
		}
		return e
	}
}

func NotᐸArgumentsᐳ(rule func(Arguments) error, newError error) func(Arguments) error {
	return func(s Arguments) error {
		e := rule(s)
		if e == nil {
			return newError
		}
		return nil
	}
}

var MetaᐸArgumentsᐳ = struct {
	SourcePackage      string
	SourceType         string
	OutputFileName     string
	OutputFolderPath   string
	OutputPackage      string
	OutputPackageName  string
	Client             string
	Debug              string
	SupportsANSI       string
	OnlyAsciiTypeNames string
	Workdir            string
}{
	SourcePackage:      "source-package",
	SourceType:         "source-type",
	OutputFileName:     "output-file-name",
	OutputFolderPath:   "output-folder-path",
	OutputPackage:      "output-package",
	OutputPackageName:  "output-package-name",
	Client:             "client",
	Debug:              "debug",
	SupportsANSI:       "supports-ansi",
	OnlyAsciiTypeNames: "only-ascii-type-names",
	Workdir:            "workdir",
}

// Code generated by gitlab.com/loxe-tools/go-validation v1.0.39, DO NOT EDIT.
