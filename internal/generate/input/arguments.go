package input

//go:generate go run gitlab.com/loxe-tools/go-validation/internal/generate/input/generate --debug --source-type Arguments --source-package gitlab.com/loxe-tools/go-validation/internal/generate/input

type Arguments struct {
	SourcePackage      string
	SourceType         string
	OutputFileName     string
	OutputFolderPath   string
	OutputPackage      string
	OutputPackageName  string
	Client             bool
	Debug              bool
	SupportsANSI       bool
	OnlyAsciiTypeNames bool
	Workdir            string
}

var DefaultArguments = Arguments{
	"",
	"",
	"validation",
	"",
	"",
	"",
	false,
	false,
	true,
	false,
	"./",
}
