package input

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"golang.org/x/tools/go/packages"
	"path/filepath"
)

func Normalize(arguments Arguments, log *logCLI.LogCLI) Arguments {
	if !filepath.IsAbs(arguments.Workdir) {
		absWorkdir, e := filepath.Abs(arguments.Workdir)
		if e != nil {
			log.Fatal("%v", e)
		}
		arguments.Workdir = absWorkdir
		log.Debug("--%s path converted to it's absolute path equivalent", MetaᐸArgumentsᐳ.Workdir)
	}

	loaderCfg := &packages.Config{Mode: packages.NeedName | packages.NeedFiles, Dir: arguments.Workdir}
	sourcePkgs, e := packages.Load(loaderCfg, arguments.SourcePackage)
	if e != nil {
		log.Fatal("%v", e)
	}
	if len(sourcePkgs) != 1 {
		log.Fatal("You can only parse one package at a time...")
	}
	if len(sourcePkgs[0].Errors) != 0 {
		msg := ""
		for _, err := range sourcePkgs[0].Errors {
			msg += "\n" + err.Error()
		}
		log.Fatal("The package contain errors: %s", msg)
	}

	if !strIsOnlyWhiteSpace(arguments.OutputFolderPath) && !filepath.IsAbs(arguments.OutputFolderPath) {
		absPath, e := filepath.Abs(fmt.Sprintf("%s/%s", arguments.Workdir, arguments.OutputFolderPath))
		if e != nil {
			log.Fatal("%v", e)
		}

		arguments.OutputFolderPath = absPath
		log.Debug("--%s path value converted to it's absolute path equivalent", MetaᐸArgumentsᐳ.OutputFolderPath)
	}
	if strIsOnlyWhiteSpace(arguments.OutputFolderPath) {
		arguments.OutputFolderPath = filepath.Dir(sourcePkgs[0].GoFiles[0])
		log.Debug("--%s not provided, using --%s directory instead", MetaᐸArgumentsᐳ.OutputFolderPath, MetaᐸArgumentsᐳ.SourcePackage)
	}
	if strIsOnlyWhiteSpace(arguments.OutputPackage) {
		arguments.OutputPackage = sourcePkgs[0].PkgPath
		log.Debug("--%s not provided, using --%s instead", MetaᐸArgumentsᐳ.OutputPackage, MetaᐸArgumentsᐳ.SourcePackage)
	}
	if strIsOnlyWhiteSpace(arguments.OutputPackageName) {
		arguments.OutputPackageName = sourcePkgs[0].Name
		log.Debug("--%s not provided, using --%s name instead", MetaᐸArgumentsᐳ.OutputPackageName,MetaᐸArgumentsᐳ.SourcePackage)
	}

	return arguments
}

