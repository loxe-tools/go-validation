package input

import (
	"fmt"
	"golang.org/x/tools/go/packages"
	"os"
	"regexp"
	"strings"
)

func required(s string) error {
	if strIsOnlyWhiteSpace(s) {
		return fmt.Errorf("This flag is required. More info use --help")
	}
	return nil
}

func validPackage(s string) error {
	cfg := &packages.Config{Mode: packages.NeedFiles | packages.NeedSyntax}
	pkgs, e := packages.Load(cfg, s)
	if e != nil {
		return fmt.Errorf("Cannot parse the given package: %v", e)
	}
	if len(pkgs) != 1 {
		return fmt.Errorf("Cannot parse more than one package at a time")
	}
	if len(pkgs[0].Errors) != 0 {
		return fmt.Errorf("The given package contains errors: %v", pkgs[0].Errors[0])
	}

	return nil
}

func isEmpty(s string) error {
	if !strIsOnlyWhiteSpace(s) {
		return fmt.Errorf("This cannot be set. More info use --help")
	}
	return nil
}

func validGoIdentifier(s string) error {
	if !goIdentifierRegexp.MatchString(s) {
		return fmt.Errorf("This value is not a valid GO identifier (see https://golang.org/ref/spec#identifier)")
	}
	return nil
}

func validFileName(s string) error {
	if !fileNameRegexp.MatchString(s) {
		return fmt.Errorf("This value is not a valid POSIX file name")
	}
	return nil
}

func validFolderpath(s string) error {
	file, e := os.Open(s)
	if e != nil {
		return fmt.Errorf("Cannot open this path: %v", e)
	}
	defer file.Close()
	stats, e := file.Stat()
	if e != nil {
		return fmt.Errorf("Cannot check this path info: %v", e)
	}
	if !stats.IsDir() {
		return fmt.Errorf("Not a directory")
	}

	return nil
}

func notBlankIdentifier(s string) error {
	if s == "_" {
		return fmt.Errorf("A package name must not be the blank identifier (see https://golang.org/ref/spec#Package_clause)")
	}
	return nil
}

func allOrNoneOutputFlags(args Arguments) error {
	hasOutputFolder := !strIsOnlyWhiteSpace(args.OutputFolderPath)
	hasOutputPackage := !strIsOnlyWhiteSpace(args.OutputPackage)
	hasOutputPackageName := !strIsOnlyWhiteSpace(args.OutputPackageName)
	allFlags := hasOutputFolder && hasOutputPackage && hasOutputPackageName
	noneFlags := !hasOutputFolder && !hasOutputPackage && !hasOutputPackageName

	if !allFlags && !noneFlags {
		return fmt.Errorf("You must either set all Output Flags or none")
	}
	return nil
}

func outputNotEqualSource(args Arguments) error {
	if args.SourcePackage == args.OutputPackage {
		return fmt.Errorf("If the --%s is equal to the --%s, omit it", MetaᐸArgumentsᐳ.OutputPackage, MetaᐸArgumentsᐳ.SourcePackage)
	}

	return nil
}

func sourceTypeIsEmpty(args Arguments) error           { return isEmpty(args.SourceType) }
func sourceTypeValidGoIdentifier(args Arguments) error { return validGoIdentifier(args.SourceType) }

func sourceTypeExportedWhenDifferentOutput(args Arguments) error {
	if args.SourcePackage == args.OutputPackage || strIsOnlyWhiteSpace(args.SourceType) || args.Client {
		return nil
	}

	char := args.SourceType[0:1]
	charUpper := strings.ToUpper(char)
	if charUpper != char {
		meta := MetaᐸArgumentsᐳ
		return fmt.Errorf("When the --%s is different from the --%s, the --%s must be exported",
			meta.SourcePackage, meta.OutputPackage, meta.SourceType)
	}
	return nil
}

// -----

func strIsOnlyWhiteSpace(s string) bool { return strings.TrimSpace(s) == "" }

var goIdentifierRegexp = func() *regexp.Regexp {
	reg, e := regexp.Compile("^(\\pL|_)(?:\\pL|_|\\p{Nd})*$")
	if e != nil {
		panic(e)
	}
	return reg
}()

var fileNameRegexp = func() *regexp.Regexp {
	reg, e := regexp.Compile("^[-_.A-Za-z0-9]+$")
	if e != nil {
		panic(e)
	}
	return reg
}()
