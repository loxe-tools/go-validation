package input

var schemaArguments = NewSchemaᐸArgumentsᐳ().
	RootRule(rootRule).
	RootedFields(func(schema SchemaRootedFieldsᐸArgumentsᐳ) SchemaRootedFieldsᐸArgumentsᐳ {
		return schema.
			SourceType(sourceTypeRule)
	}).
	Fields(func(schema SchemaFieldsᐸArgumentsᐳ) SchemaFieldsᐸArgumentsᐳ {
		return schema.
			SourcePackage(sourcePackageRule).
			OutputFileName(outputFileNameRule).
			OutputFolderPath(outputFolderPathRule).
			OutputPackage(outputPackageRule).
			OutputPackageName(outputPackageNameRule).
			Workdir(workdirRule)
	})

// -----

var rootRule = IntersectionᐸArgumentsᐳ(
	allOrNoneOutputFlags,
	outputNotEqualSource,
)

var sourcePackageRule = Intersectionᐸstringᐳ(required, validPackage)
var sourceTypeRule = UnionᐸArgumentsᐳ(
	sourceTypeIsEmpty,
	IntersectionᐸArgumentsᐳ(sourceTypeValidGoIdentifier, sourceTypeExportedWhenDifferentOutput))

var outputFileNameRule = Intersectionᐸstringᐳ(required, validFileName)
var outputFolderPathRule = Unionᐸstringᐳ(isEmpty, validFolderpath)
var outputPackageRule = Unionᐸstringᐳ(isEmpty, validPackage)
var outputPackageNameRule = Unionᐸstringᐳ(isEmpty, Intersectionᐸstringᐳ(validGoIdentifier, notBlankIdentifier))

var workdirRule = Unionᐸstringᐳ(isEmpty, validFolderpath)
