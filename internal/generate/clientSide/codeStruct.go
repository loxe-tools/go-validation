package clientSide

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide"
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide/templates"
	"go/types"
)

func (c *clientCode) structCode(struct_ types.Object, log *logCLI.LogCLI) string {
	structCode := ""
	structTypeIdentifier := serverSide.ResolveTypeIdentifier(struct_, c.dummyFile, log)
	if c.isTypeBuilt(structTypeIdentifier) {
		log.Debug("Code for this struct already generated, go to the next one...")
		return ""
	}

	c.newTypeBuilt(structTypeIdentifier)
	structMetaVarName := templates.StructMetaVarName(structTypeIdentifier)
	structCode += fmt.Sprintf("export const %s = {\n", structMetaVarName)
	structCode += structFieldsCode(struct_.Type().Underlying().(*types.Struct), "", 1, map[string]bool{}, log)
	structCode += "}\n"

	return structCode
}
