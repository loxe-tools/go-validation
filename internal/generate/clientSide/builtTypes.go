package clientSide

func (c *clientCode) newTypeBuilt(typeIdentifier string) {
	c.builtTypes[typeIdentifier] = true
}

func (c *clientCode) isTypeBuilt(typeIdentifier string) bool {
	_, alreadyBuilt := c.builtTypes[typeIdentifier]
	return alreadyBuilt
}
