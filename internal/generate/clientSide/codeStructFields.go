package clientSide

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide/templates"
	"go/types"
	"strings"
)

func structFieldsCode(struct_ *types.Struct, prefix string, indentSize int, fields map[string]bool, log *logCLI.LogCLI) string {
	structFieldsLog := log.Debug("Generating struct code field-by-field...")
	code := ""
	indent := strings.Repeat("  ", indentSize)

	for i := 0; i < struct_.NumFields(); i++ {
		field := struct_.Field(i)
		fieldName := field.Name()
		fieldMetaName := prefix + templates.FieldNameMetafy(fieldName)
		fieldLog := structFieldsLog.Debug("Field '%s'...", fieldName)

		_, fieldAlreadyExists := fields[fieldMetaName]
		if fieldAlreadyExists {
			fieldLog.Fatal("The field '%s', is ambiguous. Fix it to continue", fieldMetaName)
		}
		fields[fieldMetaName] = true

		childStruct, isStruct := field.Type().Underlying().(*types.Struct)
		if !isStruct {
			code += fmt.Sprintf("%s'%s': '%s',\n", indent, fieldName, fieldMetaName)
			fieldLog.Debug("Not a struct, done. Go to the next one...")
			continue
		}

		structLog := fieldLog.Debug("Is a struct, additional code is required...")
		if !field.Embedded() {
			newPrefix := fmt.Sprintf("%s.", fieldMetaName)
			code += fmt.Sprintf("%s'%s': {\n", indent, fieldName)
			code += structFieldsCode(childStruct, newPrefix, indentSize+1, map[string]bool{}, log)
			code += fmt.Sprintf("%s},\n", indent)
			structLog.Debug("Struct is not embedded, done. Go to the next one...")
			continue
		}

		embeddedLog := structLog.Debug("Recursively generating code for the embedded struct field...")
		code += structFieldsCode(childStruct, prefix, indentSize, fields, embeddedLog)
	}

	return code
}
