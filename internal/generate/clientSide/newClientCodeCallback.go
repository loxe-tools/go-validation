package clientSide

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/tsFile"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide"
	"go/types"
)

func NewClientCodeCallback(fileName, outputPackageName, outputPackagePath string) (*tsFile.TsFile, func(struct_ *types.TypeName, parentLog *logCLI.LogCLI) error) {
	c := newClientCode()
	file := tsFile.NewTsFile(fileName)

	return file, func(struct_ *types.TypeName, parentLog *logCLI.LogCLI) error {
		log := parentLog.Debug("Analysing *types.Struct '%s'...", struct_.Name())
		if !serverSide.StructIsEligible(struct_, outputPackageName, outputPackagePath, log) {
			return nil
		}

		currStructCode := c.structCode(struct_, log.Debug("Generating client code for the struct '%s'...", struct_.Name()))
		file.AddCode(currStructCode)
		log.Info("Code for the struct '%s' generated", struct_.Name())
		return nil
	}
}
