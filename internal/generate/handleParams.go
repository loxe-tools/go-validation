package generate

import (
	"gitlab.com/loxe-tools/go-base-library/codeGeneration/goParser/helpers"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-validation/internal/generate/serverSide/templates"
	"gitlab.com/loxe-tools/go-validation/internal/generate/input"
)

func handleParams(arguments input.Arguments, log *logCLI.LogCLI, fieldNameMetafy ...func(string) string) input.Arguments {
	input.Validate(arguments, log.Debug("Validating arguments..."))

	normalizeLog := log.Debug("Normalizing arguments...")
	arguments = input.Normalize(arguments, normalizeLog)
	input.PrintArguments(arguments, normalizeLog)

	// Check if there's a custom field name metafier
	if len(fieldNameMetafy) > 0 {
		log.Info("Using a client-provided custom field name metafy function")
		e := templates.SetFieldNameMetafy(fieldNameMetafy[0])
		if e != nil {
			log.Fatal("%v", e)
		}
	}

	if arguments.OnlyAsciiTypeNames {
		log.Info("Using Only ASCII typeNames")
		e := templates.SetIdentifierToTypeName(helpers.IdentifierToAsciiTypeName)
		if e != nil {
			log.Fatal("%v", e)
		}
	}

	return arguments
}
