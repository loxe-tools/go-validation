module gitlab.com/loxe-tools/go-validation

go 1.14

require (
	gitlab.com/loxe-tools/go-base-library v0.0.29
	golang.org/x/tools v0.0.0-20200929223013-bf155c11ec6f
)
