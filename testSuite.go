package loxe

type TestSuite interface {
	Fatalf(msg string, args ...interface{})
	Helper()
}
