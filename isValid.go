package loxe

func (v *LoxeValidator) IsValid() bool {
	if v.rootError != nil {
		return false
	}
	for _, fieldErr := range v.fields {
		if fieldErr != nil {
			return false
		}
	}
	for _, childForm := range v.childs {
		if !childForm.IsValid() {
			return false
		}
	}

	return true
}
